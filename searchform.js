(function ($) {
    $.fn.myPlugIn = function (options) {

        // default options
        var defaults = {
            url: '',
            keyid_name: '',
            option_str_format: '',
            limit: 10
            
        };

        //merke options with defaults
        var options = $.extend(defaults, options);

        //variables:
        var arr_indexes = findIndexesInString(options['option_str_format']);
        var arr_inputs = Array();
        var option_list = Array();
        var selecteditem = 1;
        var count = 0;
        var myVar = null;  //for boundance (timeout)
        var obj = null;
        
        // the plugin
        return this.each(function () {
            obj = $(this);
            init(obj);
        });

        
        //callback
        
        function onSelected(id){
            //alert(id)
        }
        


        //functions:
      
        function init(obj) {
            arr_indexes = findIndexesInString(options['option_str_format']);

            $(obj).wrap('<div class="dropdown">' +
                    '<div class="dropdown-content">' +
                    '<ul><li></li></ul>' +
                    '</div></div>');
            $(obj).keyup(function (e) {
                if (myVar) {
                    clearTimeout(myVar);
                }
                if (e.keyCode === 13) { //enter
                    optionSelecting();
                } else if (e.keyCode === 38 && selecteditem > 1) {//up
                    selecteditem--;
                    overitem();
                } else if (e.keyCode === 40 && selecteditem < $(".dropdown-content ul li").length - 1) {//down
                    selecteditem++;
                    overitem();
                } else {
                    selecteditem = 1;
                    overitem();

                    if ($(obj).val().length >= 1) {

                        myVar = setTimeout(function () {
                            var insertedtext = encodeURIComponent($(obj).val())
                            $.getJSON(options['url'] + insertedtext + "/" + options['limit'], function (result) {
                                arr_inputs = result;
                                showOptions();
                                //$("#debug").html("")
                            });
                        }, 500);




                    } else if ($(obj).val().length == 0) {
                        arr_inputs = Array()
                        showOptions();
                    }
                }
            });

        }


        function optionSelecting() {
           
            if (option_list.length && option_list[selecteditem - 1] !== 'undefined') {
                $(obj).val(option_list[selecteditem - 1]);
                $(obj).parentsUntil(".dropdown-content").parent().find("ul li:not(:first)").remove();
                var selected_key_value = arr_inputs[selecteditem - 1][options['keyid_name']];

                var attr_name = $(obj).attr('name');
                if (attr_name !== 'undefined') {
                    $(obj).removeAttr('name');
                    $(obj).parent().parent().append('<li><input type="hidden" name="' + attr_name + '" value="' + selected_key_value + '" /></li>');
                }
                //here callback function call:
                onSelected(selected_key_value);
            }

        }



        function createOptionList() {
            var option = "";
            option_list = Array();
            for (var i = 0; i < arr_inputs.length; i++) {
                option = options['option_str_format'];
                for (var j = 0; j < arr_indexes.length; j++) {

                    option = option.replace("{{" + arr_indexes[j] + "}}", arr_inputs[i][arr_indexes[j]]);
                }
                option_list.push(option);
            }
        }

        function showOptions() {

            createOptionList();

            $(".dropdown-content ul li").not(':eq(0)').remove();

            for (var i = 0; i < option_list.length; i++) {
                $(".dropdown-content ul").last("li").append(
                        "<li><div>" + option_list[i] + "</div></li>"
                        );
            }
            overitem();
            $(".dropdown-content ul li div").bind("click", function () {
                selecteditem = $(this).parent().index();
                optionSelecting();

            });
        }


        function overitem() {
            $(".dropdown-content ul li").removeClass("over-item");
            $(".dropdown-content ul li").eq(selecteditem).addClass("over-item");
        }



        function findIndexesInString(option_str_format) {

            var indexes = Array();
            var pos = 0;

            while ((pos = option_str_format.indexOf("{{", pos)) !== -1) {
                pos += 2;
                indexes.push(option_str_format.substring(pos, option_str_format.indexOf("}}", pos)));
            }
            return indexes;
        }

    };
})(jQuery);

